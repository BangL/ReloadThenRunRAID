_G.ReloadThenRun = _G.ReloadThenRun or {}
if not _G.ReloadThenRun.setup then
    ReloadThenRun.setup = true

    ReloadThenRun._defaults_path = ModPath .. "defaults.json"
    ReloadThenRun._options_path = SavePath .. "ReloadThenRun.json"
    ReloadThenRun._defaults = {}
    ReloadThenRun._options = {}

    function ReloadThenRun:LoadDefaults()
        local default_file = io.open(self._defaults_path, "r")
        if default_file then
            self._defaults = json.decode(default_file:read("*all"))
            default_file:close()
        end
    end

    function ReloadThenRun:Load()
        self._options = deep_clone(self._defaults)
        local file = io.open(self._options_path, "r")
        if file then
            local config = json.decode(file:read("*all"))
            file:close()
            if config and type(config) == "table" then
                for k, v in pairs(config) do
                    self._options[k] = v
                end
            end
        end
    end

    function ReloadThenRun:Save()
        local file = io.open(self._options_path, "w+")
        if file then
            file:write(json.encode(self._options))
            file:close()
        end
    end

    function ReloadThenRun:GetDefault(id)
        return self._defaults[id]
    end

    function ReloadThenRun:GetOption(id)
        return self._options[id]
    end

    function ReloadThenRun:SetOption(id, value, save)
        self._options[id] = value
        if save then
            self:Save()
        end
    end

    ReloadThenRun:LoadDefaults()
    ReloadThenRun:Load()
end
