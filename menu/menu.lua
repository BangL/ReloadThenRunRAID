ReloadThenRunMenu = ReloadThenRunMenu or class(BLTMenu)

function ReloadThenRunMenu:Init(root)
    self:Title({
        text = "rtr_options",
    })
    self:Label({
        text = nil,
        localize = false,
        h = 8,
    })

    self:Slider({
        name = "rtr_options_block_after",
        text = "rtr_options_block_after_title",
        min = 0.0,
        max = 1.0,
        step = 0.1,
        default_value = ReloadThenRun:GetDefault("block_after_pct"),
        value = ReloadThenRun:GetOption("block_after_pct"),
        callback = callback(self, self, "block_after_pct_changed"),
    })

    self:LongRoundedButton2({
        name = "rtr_options_reset",
        text = "rtr_options_reset_title",
        localize = true,
        callback = callback(self, self, "Reset"),
        ignore_align = true,
        y = 832,
        x = 1472,
    })
end

function ReloadThenRunMenu:block_after_pct_changed(value)
    ReloadThenRun:SetOption("block_after_pct", value, false) -- only save when closing menu, saving here would break :Reset()!
end

function ReloadThenRunMenu:Reset(value, item)
    QuickMenu:new(
        managers.localization:text("rtr_options_reset_title"),
        managers.localization:text("rtr_options_reset_confirm"),
        {
            [1] = {
                text = managers.localization:text("dialog_no"),
                is_cancel_button = true,
            },
            [2] = {
                text = managers.localization:text("dialog_yes"),
                callback = function()
                    ReloadThenRun:LoadDefaults()
                    self:ReloadMenu()
                    ReloadThenRun:Save()
                end,
            },
        },
        true
    )
end

function ReloadThenRunMenu:Close()
    ReloadThenRun:Save()
end

Hooks:Add("MenuComponentManagerInitialize", "ReloadThenRun.MenuComponentManagerInitialize", function(self)
    RaidMenuHelper:CreateMenu({
        name = "rtr_options",
        name_id = "rtr_options",
        inject_menu = "blt_options",
        class = ReloadThenRunMenu
    })
end)
